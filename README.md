This is an implementation of a CSV parser in rust.

Current architecture: 
- large up front allocations and subsequent reads from file system to prevent memory fragmentation.
- parallel parsing w/o rayon.
- tracks row statistics to improve memory allocation during memory serialization.
- vectorized/columnar in memory representation of csv.
- parser is magnitudes faster than pandas, parsing is 3-6x times faste than polars.

TODO- 
- implement dataframe and column struct.
- implement simple operations on dataframe.

Chores-
- make the naming convention not terrible/fix modules and file structure, make library not binary.
- collate parsed chunks.
- improve front end apis/interfaces.