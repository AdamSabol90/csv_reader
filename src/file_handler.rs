use std::path::Path;
use std::fs::File;
use std::io::{ BufReader, Read, Seek, SeekFrom};
use std::io;
use std::fs;
use std::cmp::{min,max};

pub struct FileHandler<P: AsRef<Path>> {
    reader: BufReader<File>,
    path: P,
    file_metadata: fs::Metadata,
    chunk_size: usize,
    pub start_of_chunk: usize,
    pub end_of_chunk: usize, 
    pub cursor: usize,
    read_size:usize,
}

impl<P: AsRef<Path>> FileHandler<P> {
    pub fn new(file_path: P) -> io::Result<Self> {
        let file = File::open(file_path.as_ref())?;
        let file_metadata = fs::metadata(file_path.as_ref())?; 
        Ok(FileHandler { 
            reader: BufReader::new(file),
            path: file_path, 
            file_metadata, 
            chunk_size : Default::default(),
            start_of_chunk: Default::default(),
            end_of_chunk: Default::default(),
            cursor: Default::default(), 
            read_size: Default::default(),
        })
    }

    pub fn calc_chunk_size(&mut self) -> &mut Self {
        self.chunk_size = self.file_metadata.len() as usize/num_cpus::get();
        self
    }

    pub fn seek(&mut self, pid: usize) -> io::Result<&mut Self> {
        self.start_of_chunk = self.chunk_size * pid;
        self.cursor = self.start_of_chunk;
        self.end_of_chunk = (self.start_of_chunk + self.chunk_size)-1;
        self.reader.seek(SeekFrom::Start(self.start_of_chunk as u64))?;
        Ok(self)
    }

    pub fn calc_read_size(&mut self) -> &mut Self {
        let read_size = max(self.chunk_size / 50, 1_000_000);
        self.read_size = read_size;
        self
    }
    
    pub fn create_buffer(&mut self) -> Vec<u8> {
        let mut buffer = Vec::with_capacity(self.chunk_size);    
        buffer.resize(self.chunk_size, 0);
        buffer  
    }

    pub fn get_this_read(&mut self) -> usize{
        let mut this_read: usize = Default::default();

        if (self.cursor + self.read_size) > self.end_of_chunk {
            this_read = self.end_of_chunk-self.cursor;  
        }else{
            this_read = self.read_size;
        }
        self.cursor += this_read; 
        this_read 
       //let indices = (self.cursor,self.cursor+this_read); 

       //(self.cursor,self.cursor+this_read)
    }

    pub fn read_into_buffer(&mut self, buffer: &mut [u8]) -> io::Result<()>{
        let _read_bytes = self.reader.read(buffer)?;
        Ok(())
    }
}
