use std::string::String;

pub trait ValidColumnType {}
impl ValidColumnType for u64 {}
impl ValidColumnType for f64 {}
impl ValidColumnType for String {}

pub enum ColType {
    U64Column,
    Float64Column,
    StringColumn,
}

pub struct Column{
     
}
impl Column{
    pub fn new(data_type: ColType) {
        
    }
}
