use crate::file_handler::FileHandler;
use std::{thread, u8}; 
use std::fs::File; 
use std::io::{self, BufRead};

#[derive(Clone, Copy)]
struct EncodingKeys{
    delim: u8, 
    newline: u8,
    quote: u8,
}
impl EncodingKeys{ 
    fn new(delim: u8, newline: u8, quote: u8) -> Self{
        EncodingKeys{delim, newline, quote}
    }
    fn from_utf8_chars(delim: char, newline :char, quote: char) -> Self { 
        let (delim, newline, quote) = (delim as u8, newline as u8, quote as u8);
        assert!(delim.is_ascii() && newline.is_ascii() && quote.is_ascii(), "Only ascii characters allowed as delimiter, newline, or quote");
        EncodingKeys { delim, newline, quote }
    }
}

struct Parser{
    cursor: usize, 
    delim_index: usize, 
    col_num: usize, 
    last_newline: usize,
    escaped: bool,
}
impl Parser{
    fn new() -> Self{
        Parser{
        cursor: 0,
        delim_index: 0,
        col_num: 0,
        last_newline: 0,
        escaped: false } 
    }

    fn push_row<'a>(&mut self, virt_matrix: &mut VirtualColMatrix<'a>, buffer: &'a [u8]) {
        let row_val = &buffer[self.delim_index..self.cursor];
        let str_repr = std::str::from_utf8(row_val).unwrap_or_else(|e| {
            println!("Error converting to utf8: {}", e);
            ""
        });
        if self.col_num >= virt_matrix.matrix.len(){
            return
        }
        virt_matrix.matrix[self.col_num].push(str_repr); 
    }

    fn delim_match(&mut self) {
        self.delim_index = self.cursor + 1;
        self.col_num += 1; 
        self.cursor += 1
    }

    fn newline_match(&mut self) {
        self.delim_index = self.cursor+1; 
        self.last_newline = self.cursor;
        self.col_num = 0;
        self.cursor += 1; 
    }

    fn escape_match(&mut self) {
        self.escaped = !self.escaped;
        self.cursor +=1; 
    }

    fn other_byte(&mut self) {
        self.cursor +=1; 
    }
}

struct VirtualColMatrix<'a> {
    overflow: Option<Vec<&'a[u8]>>,
    matrix: Vec<Vec<&'a str>>,
}

impl<'a> VirtualColMatrix<'a> {
    fn new(initialization: usize, num_cols: usize ) -> Self {
        let matrix: Vec<Vec<&'a str>> = vec![Vec::with_capacity(initialization); num_cols];
        VirtualColMatrix {overflow:Some(Vec::with_capacity(2)), matrix,}
    }

    fn catch_overflow(&mut self, buffer: &'a [u8], desc: bool) -> &'a[u8]{
        let mut i =  if desc {buffer.len()-1} else {0}; 
        while (buffer[i] !=('\n' as u8))&(i <= buffer.len()-1) {
            if desc {i-=1} else {i+=1}
        }
        let (overflow, remain) = buffer.split_at(i);

        if let Some(ref mut overflow_vec) = &mut self.overflow{
            overflow_vec.push(overflow);
        }
        else{
            self.overflow = Some(vec![overflow]);
        }

        remain
    }

    fn parse_bytes(&mut self, buffer : &'a [u8], encoding_keys: & EncodingKeys){
        let mut parser = Parser::new();
        while parser.cursor < buffer.len(){
            match &buffer[parser.cursor]{
                _ if (buffer[parser.cursor] == encoding_keys.delim) && !parser.escaped => {
                    parser.push_row(self, buffer);
                    parser.delim_match();
                }
                _ if (buffer[parser.cursor] == encoding_keys.newline) && !parser.escaped => {
                    parser.push_row(self, buffer);
                    parser.newline_match();
                }
                _ if (buffer[parser.cursor] == encoding_keys.quote) => {
                    parser.escape_match();
                }
                _ => {
                    parser.other_byte();
                }
            }
        }
        self.catch_overflow(buffer, true);
        for i in 0..parser.col_num{
            self.matrix[i].pop();
        }
        }
    fn squash_matrices(matrices: Vec<VirtualColMatrix>){
    }
}
//TODO
//handle overflows 

fn get_header<P>(path: P, delim: char) -> std::io::Result<Vec<String>>
where
    P: AsRef<std::path::Path>,
{
    let file = File::open(path)?;
    let reader = io::BufReader::new(file);
    let mut columns = Vec::new();

    if let Some(Ok(first_line)) = reader.lines().next() {
        let mut last_pos = 0;
        for (i, c) in first_line.char_indices() {
            if c == delim {
                if last_pos != i {
                    columns.push(first_line[last_pos..i].to_string());
                }
                last_pos = i + 1; 
            }
        }
        if last_pos < first_line.len() {
            columns.push(first_line[last_pos..].to_string());
        }
    }
    Ok(columns)
}

pub fn read_csv<P>(file_path: P) -> std::io::Result<()>
    where
        P: AsRef<std::path::Path> + Send + Clone + 'static,
    {
    let mut handles = Vec::new();
    let header = get_header(file_path.clone(), ',')?; 
    for i in 0..(num_cpus::get()){
        let file_path_clone = file_path.clone();
        let mut row_agg: (usize,usize) = (0,0);
        let header_clone = header.clone();

        let handle = thread::spawn(move || {
            let mut file_handler = FileHandler::new(file_path_clone).expect("Should be able to make file_handler");
            file_handler
                .calc_chunk_size()
                .seek(i).expect("Should be able to seek.")
                .calc_read_size();

            let mut buffer = file_handler.create_buffer();
            let mut buffer_slice: &mut [u8] = buffer.as_mut_slice();
            let encoding_keys = EncodingKeys::from_utf8_chars(',', '\n', '"');
            let mut matrices = Vec::with_capacity(50);
            while file_handler.cursor < file_handler.end_of_chunk {

                let read_size = file_handler.get_this_read();
                let (this_buffer, remain) = buffer_slice.split_at_mut(read_size);
                buffer_slice = remain; 
                let _ = file_handler.read_into_buffer(this_buffer);
                let mut iteration_matrix = VirtualColMatrix::new((row_agg.0)
                    .checked_div(row_agg.1)
                    .unwrap_or_default(),
                    header_clone.len()
                );
                let filtered_buffer = iteration_matrix.catch_overflow(this_buffer, false); 
                iteration_matrix.parse_bytes(filtered_buffer, &encoding_keys); 
                row_agg.0 += iteration_matrix.matrix[0].len(); 
                row_agg.1 += 1;
                matrices.push(iteration_matrix);
            }
        });
       handles.push(handle)
    };
    for handle in handles {let _ = handle.join();}
   Ok(())
}
