use std::collections::HashMap; 
use crate::column::{Column, ValidColumnType}; 

pub struct DataFrame<T: ValidColumnType>{
    pub columns: HashMap<String, Column<T>>
}

impl DataFrame <T> {
    pub fn new(header: Vec<String>, initial_size: Option<usize>) -> Self {
        DataFrame{columns : HashMap::new()}  
    }
}
