mod file_handler;
mod csv_reader; 
//mod dataframe;
//mod column;
use std::io;
use std::time::Instant;

fn main() -> io::Result<()>{
    let path = "/home/adam/projects/csv_reader/test_data/Books_rating.csv";
    let start = Instant::now();
    {
        let _data = std::fs::read(path)?; 
    }
    let duration = start.elapsed();
    println!("Time taken to read file: {:?}", duration);

    let start = Instant::now();
    {
        let _df = csv_reader::read_csv(path);
    }
    let duration = start.elapsed();
    println!("Time taken to read file: {:?}", duration);

    Ok(())
}

